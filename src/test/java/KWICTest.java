import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class KWICTest {

    @Test
    void TestIgnoredWord() {
        String input = "is\n" +
                "the\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        Set<String> expected = new HashSet<String>();
        expected.add("is");
        expected.add("the");
        assertEquals(new KWIC().getIgnore(input), expected);
    }

    @Test
    void TestSentence(){
        String words = "the ascent of man";
        String[] s=words.split(" ");
        String expected = "the ASCENT of man";
        assertEquals(new KWIC().getSentence(s,1), expected);
    }

    @Test
    void TestMap(){
        Set<String> ignoreSet = new HashSet<String>();
        ignoreSet.add("is");
        ignoreSet.add("the");
        ignoreSet.add("of");
        ignoreSet.add("and");
        ignoreSet.add("as");
        ignoreSet.add("a");
        ignoreSet.add("but");
        String[] Sentences = {"descent of man", "the ascent of Man"};
        ArrayList<String> a = new ArrayList<>();
        ArrayList<String> d = new ArrayList<>();
        ArrayList<String> m = new ArrayList<>();
        d.add("DESCENT of man");
        a.add("the ASCENT of man");
        m.add("descent of MAN");
        m.add("the ascent of MAN");
        Map<String,ArrayList<String>>expected = new HashMap<String,ArrayList<String>>();
        expected.put("descent",d);
        expected.put("ascent",a);
        expected.put("man",m);
        assertEquals(new KWIC().getMap(ignoreSet,Sentences), expected);
    }

}