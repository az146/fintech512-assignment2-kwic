import java.util.*;
import java.util.HashMap;


public class KWIC {

    public static Set<String>getIgnore(String input){
        Set<String>set = new HashSet<String> ();
        String[] s = input.split("::\n");
        String[] ignores = s[0].split("\n");
        for(String str:ignores){
            set.add(str.toLowerCase());
        }
        return set;
    }

    public static String getSentence(String[] words, int idx){
        String ans="";
        for(int i=0;i<words.length;i++){
            if(i==idx){
                ans=ans+words[i].toUpperCase();
            }
            else{
                ans=ans+words[i].toLowerCase();
            }
            if(i!=words.length-1){
                ans=ans+" ";
            }
        }
        return ans;
    }
    public static HashMap<String, ArrayList<String>>getMap(Set<String> ignore, String[] sentences){
        HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
        for(String sen:sentences){
            String[] words=sen.split(" ");
            for(int i=0;i<words.length;i++){
                if(ignore.contains(words[i].toLowerCase())){
                    continue;
                }
                else{
                    String edit=getSentence(words,i);
                    if(!map.containsKey(words[i].toLowerCase())){
                        map.put(words[i].toLowerCase(), new ArrayList<String>());
                    }
                    map.get(words[i].toLowerCase()).add(edit);
                }
            }
        }
        return map;
    }

    public static void printout(ArrayList<String>keywords,HashMap<String, ArrayList<String>>map){
        for(String keyword:keywords){
            ArrayList<String> sentences=map.get(keyword);
            for(String sen:sentences){
                System.out.println(sen);
            }
        }
    }

    public static void main(String[] args) {
        String input = "is\n" + "the\n" + "of\n" + "and\n" + "as\n" +
                "a\n" + "but\n" + "::\n" + "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";

        String[] arrayInput = input.split("::\n");
        // set of all ignored words
        Set<String>ignoreSet=getIgnore(arrayInput[0]);
        // list of all sentences
        String[] sentences=arrayInput[1].split("\n");
        // get Map
        HashMap<String, ArrayList<String>>map=getMap(ignoreSet,sentences);
        // get keywords from keyset of map and sort
        ArrayList<String>keywords=new ArrayList<String>(map.keySet());
        Collections.sort(keywords);
        // print out the final output
        printout(keywords,map);
    }
}